package com.bhlangonijr.service;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    private static ArrayList<Message> messages = new ArrayList<Message>();
    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
    public Response send(Message message) throws Exception {
        Response response = new Response();

        if(message.getFrom().split("@")[1].equals(message.getTo().split("@")[1])) {
            response.setSuccess(true);
            response.setMessageId(message.getId());
            response.setText(message.getText() + " - has been processed");
        }

        if (response.getSuccess()) {
            messages.add(message);
            log.info("{} - has been processed", message.getText());
        }
        else throw new Exception("Message cannot be sourced by martians!");
        return response;
    }

    public List<Message> getAllMessages() {
        return messages;
    }

    public Message getById(String id) {

        for (Message m: messages) {
            if(m.getId().equals(id))
                return m;
        }
        return null;
    }


}
