package com.bhlangonijr;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest(randomPort = true)
@DirtiesContext
public class MessageServiceTest {

    @Value("${local.server.port}")
    private int port;

    @Test
    public void testHappyPathMessageSend() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // 1) Send a message sourced by ´@earth´
        Message message = new Message("123", "me@earth", "you@earth", "hello there");

        HttpEntity<Message> request = new HttpEntity<>(message, headers);
        ResponseEntity<Response>  response = new TestRestTemplate().postForEntity(
                "http://localhost:" + port + "/message/send.json", request, Response.class);

        assertTrue(response.getBody().getSuccess());
        assertEquals("hello there - has been processed", response.getBody().getText());

        // 2) now check if the message that we just sent is there
        ResponseEntity<Message[]> response2 = new TestRestTemplate().getForEntity(
                "http://localhost:" + port + "/message/all.json", Message[].class);

        assertEquals(1, response2.getBody().length);
        assertEquals((""+message).trim(), (""+response2.getBody()[0]).trim());

    }

    @Test
    public void testInvalidMessageSend() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // 1) Send a message sourced by ´@earth´
        Message message = new Message("123", "me@mars", "you@earth", "hello there");

        HttpEntity<Message> request3 = new HttpEntity<>(message, headers);
        ResponseEntity<Response>  response3 = new TestRestTemplate().postForEntity(
                "http://localhost:" + port + "/message/send.json", request3, Response.class);

        assertFalse(response3.getBody().getSuccess());
        assertEquals("Message cannot be sourced by martians!", response3.getBody().getText());

    }
}
